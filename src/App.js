


import React from "react";


import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";


import DashBoard from "./components/DashBoard";

import LienHe from "./components/LienHe";
import Login from "./components/Login";
import PrivateRouter from './components/PrivateRouter'
import CreateProduct from "./pages/create/CreateProduct";
import AllProduct from "./pages/listproduct/AllProduct";


function App() {
  return (
    <Router>
        <div className="App">
            <AllProduct/>
            <CreateProduct/>
          <Routes>
            <Route  path="/dashboard" element={<DashBoard/>}></Route>
            <Route path="/lienhe" element={<PrivateRouter><LienHe/></PrivateRouter>}/>
            <Route path="/login" element={<Login/>}></Route>
          </Routes>

      </div>
    </Router>
    
  );
}

export default App;
