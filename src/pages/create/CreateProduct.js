import React from 'react'
import style from './style/FormCreatedPage.module.scss'
const CreateProduct = () => {
    const [linkImg, setlinkImg] = React.useState("");
  return (
    <div className={style.form_page}>

        <div className={style.left}>
               
                <div  className={style.left_img}>
                     <img src={linkImg} alt=""></img>
                </div>
                <div  className={style.left_input}>
                    <input type={"text"} placeholder='Nhậpp Link ảnh' onChange={(e)=>setlinkImg(e.target.value)}></input>
                </div>
                
        </div>
        <div className={style.right}>
                <div className={style.content_info}>
                    <div className={style.content_info_left}>
                            <div className={style.content_item}>
                                <span>Name</span>
                                <input type={'text'}
                                    placeholder={"Tên của sản phẩm"}
                                    required={true}
                                    defaultValue={""}
                                />
                            </div>
                            <div className={style.content_item}>
                                <span>Name</span>
                                <input type={'text'}
                                    placeholder={"Tên của sản phẩm"}
                                    required={true}
                                    defaultValue={""}
                                />
                            </div>
                            <div className={style.content_item}>
                                <span>Name</span>
                                <input type={'text'}
                                    placeholder={"Tên của sản phẩm"}
                                    required={true}
                                    defaultValue={""}
                                />
                            </div>
                            <div className={style.content_item}>
                                <span>Name</span>
                                <input type={'text'}
                                    placeholder={"Tên của sản phẩm"}
                                    required={true}
                                    defaultValue={""}
                                />
                            </div>
                            <div className={style.content_item}>
                                <span>Name</span>
                                <input type={'text'}
                                    placeholder={"Tên của sản phẩm"}
                                    required={true}
                                    defaultValue={""}
                                />
                            </div>
                            <div className={style.content_item}>
                                <span>Name</span>
                                <input type={'text'}
                                    placeholder={"Tên của sản phẩm"}
                                    required={true}
                                    defaultValue={""}
                                />
                            </div>
                    </div>
                    <div className={style.content_info_right}>
                    <div className={style.content_item}>
                                <span>Name</span>
                                <input type={'text'}
                                    placeholder={"Tên của sản phẩm"}
                                    required={true}
                                    defaultValue={""}
                                />
                            </div>
                            <div className={style.content_item}>
                                <span>Name</span>
                                <input type={'text'}
                                    placeholder={"Tên của sản phẩm"}
                                    required={true}
                                    defaultValue={""}
                                />
                            </div>
                            <div className={style.content_item}>
                                <span>Name</span>
                                <input type={'text'}
                                    placeholder={"Tên của sản phẩm"}
                                    required={true}
                                    defaultValue={""}
                                />
                            </div>
                            <div className={style.content_item}>
                                <span>Name</span>
                                <input type={'text'}
                                    placeholder={"Tên của sản phẩm"}
                                    required={true}
                                    defaultValue={""}
                                />
                            </div>
                            <div className={style.content_item}>
                                <span>Name</span>
                                <input type={'text'}
                                    placeholder={"Tên của sản phẩm"}
                                    required={true}
                                    defaultValue={""}
                                />
                            </div>
                            <div className={style.content_item}>
                                <span>Name</span>
                                <input type={'text'}
                                    placeholder={"Tên của sản phẩm"}
                                    required={true}
                                    defaultValue={""}
                                />
                            </div>
                    </div>
                </div>
                <div className={style.action_create}>
                    <button>Created</button>
                    <button>Cancel</button>
                </div>
        </div>
    {/* <form className={style.form_content}>
        <div className={style.title}><h2> Created Product</h2></div>
        <div className={style.content}>
            <div className={style.content_item}>
                <span>Name</span>
                <input type={'text'}
                       placeholder={"Tên của sản phẩm"}
                       required={true}
                       defaultValue={""}
                />
            </div>
            <div className={style.content_item}>
                <span>CPU</span>
                <input type={'text'}
                       placeholder={"Thông Tin Cpu"}
                      
                       required={true}
                       defaultValue={""}
                />
            </div>
            <div className={style.content_item}>
                <span>Price</span>
                <input type={'number'}
                       placeholder={"Giá của sản phẩm"}
                     
                       required={true}
                       defaultValue={""}
                />
            </div>
            <div className={style.content_item}>
                <span>Ổ cứng </span>
                <input type={'text'}
                       placeholder={"Thông tin ổ cứng "}
                    
                       required={true}
                       defaultValue={""}
                />
            </div>
            <div className={style.content_item}>
                <span>Ram</span>
                <input type={'text'}
                       placeholder={"Thông tin ram"}
                      
                       required={true}
                       defaultValue={""}
                />
            </div>
            <div className={style.content_item}>
                <span>Graphic Card</span>
                <input type={'text'}
                       placeholder={"Thông tin Card đồ họa" }
                    
                       required={true}
                       defaultValue={""}
                />
            </div>
            <div className={style.content_item}>
                <span>Monitor</span>
                <input type={'text'}
                       placeholder={"Thông tin màn hình "}
                      
                       required={true}
                       defaultValue={""}
                />
            </div>
            <div className={style.content_item}>
                <span>Battery</span>
                <input type={'text'}
                       placeholder={"Dung lượng pin"}
                      
                       required={true}
                       defaultValue={""}
                />
            </div>
            <div className={style.content_item}>
                <span>Color</span>
                <input type={'text'}
                       placeholder={"Màu sắc"}
                     
                       required={true}
                       defaultValue={""}
                />
            </div>
            <div className={style.content_item}>
                <span>Size</span>
                <input type={'text'}
                       placeholder={"Kích thước của máy"}
                      
                       required={true}
                       defaultValue={""}
                />
            </div>
            <div className={style.content_item}>
                <span>Weight</span>
                <input type={'text'}
                       placeholder={"Cân nặng của máy "}
                    
                       required={true}
                       defaultValue={""}
                />
            </div>
            <div className={style.content_item}>
                <span>isStock</span>
                <input type={'checkbox'}

                       


                />
            </div>
            <div className={style.content_item}>
                <span>IMG</span>
                <input type={'text'}
                       placeholder={"Link ảnh"}
                     
                       required={true}
                       defaultValue={""}
                />
            </div>
        </div>
        <div className={style.action} >
            <button type={'submit'} >Created</button>
            <button type={'submit'}>Cancel</button>
        </div>
    </form> */}
</div>
  )
}

export default CreateProduct