import React from 'react'
import style from './style/listproduct.module.scss'
import db from './fakedb'
const AllProduct = () => {


    const [arrState,setArrState]=React.useState(()=>[{btn1:"#ffffff"},{btn2:"#333"}])

    const [number, setnumber] = React.useState(1);
    const listBtn=(length)=>{
        let arr=[];
        for (let index = 0; index < length; index++) {
       
            arr.push( <li>
                <button id={`${index}`} onClick={()=>{setnumber(index); changColor(index,length)}} >{index}</button>
           </li>);
             
         }
        return arr;
    }
    const changColor=(number,length)=>{
        for (let index = 0; index < length; index++) {
            if(index===number){
                document.getElementById(`${number}`).style.backgroundColor="#333";
                document.getElementById(`${number}`).style.color="#ffffff";
            }
            else{
                document.getElementById(`${index}`).style.backgroundColor="#ffffff";
                document.getElementById(`${index}`).style.color="#333";
            }
          
   
            
        }
       
    }
    const handleClick=(arr)=>{
      
        //ar là list các btn
        for (let index = 0; index < arr.length; index++) {
       
           arr[index].style.backgroundColor = "#333";
            
        }
    }
  return (
    <div className={style.list_product}>
        <div className={style.action_create}>
              <button><i className="fa-solid fa-plus"></i>Tạo mới</button>
        </div>
        <div className={style.list_table}>
        <table className={style.table}>
                <thead className={style.table_head}>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Name</th>
                        <th scope="col">CPU</th>
                        <th scope="col">Ram</th>
                        <th scope="col">Hard_Disk</th>
                        <th scope="col">Graphic</th>
                        <th scope="col">Monitor</th>
                        <th scope="col">Battery</th>
                        <th scope="col">Price</th>
                        <th scope="col">Weight</th>
                        <th scope="col">Size</th>
                        <th scope="col">Color</th>
                        <th scope="col">IMG</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody className={style.table_body}>
                            {db.map((value,index)=>(
                                <tr  className={index%2!==0?style.style_Background:""} >
                                <th scope="row">{value.a1}</th>
                                <td className={style.style_font_yellow}>{value.a1}</td>
                                <td>{value.a1}</td>
                                <td>{value.a1}</td>
                                <td className={style.style_font_blue}>{value.a1}</td>
                                <td >{value.a1}</td>
                                <td>{value.a1}</td>
                                <td>{value.a1}</td>
                                <td  className={style.style_font_yellow}>{value.a1}</td>
                                <td>{value.a1}</td>
                                <td>{value.a1}</td>
                                <td>{value.a1}</td>
                                <td className={style.action_button}>
                                    <img className={""} src={value.a1} alt={"Hinh ảnh"}/>
                                   </td>
                                <td>
                                    <div className={""}>
                                        <button value={"value.id"} >Update</button>
                                        <button className={""}>
                                      
                                          Delete
                                        </button>
                                    </div>

                                </td>
                            </tr>
                            ))}
                         
                    
            
                </tbody>
            </table>
        </div>
        <div className={style.list_btn}>
                <div className={style.list_btn_left}>
                    <ul>
                        <li>
                             <button id='a' onClick={()=>setnumber()} ><i className="fa-solid fa-angles-left"></i></button>
                        </li>
                        <li>                   
                            <button ><i className="fa-solid fa-angle-left"></i></button>
                                                                        
                        </li>
                        {listBtn(9).map((value,index)=>value)}
                        <li>
                            <button><i className="fa-solid fa-angle-right"></i></button>
                        </li>
                        <li>
                            <button ><i className="fa-solid fa-angles-right"></i></button>
                        </li>
                       
                    </ul>
                </div>
                <div className={style.list_btn_right}>
                    <span>Tong cong: 20</span>
                </div>
                
        </div>
        
    </div>
  )
}

export default AllProduct